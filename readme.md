# Docs
Some links to docs and community chat.
- https://atproto.com/
- https://www.npmjs.com/package/@atproto/api
- https://matrix.to/#/#bluesky-dev:matrix.org

# Development
This project uses `bun` instead of `node`.

## local development server
To run a local development server:
- `bun install`
- `npm run dev`

## build for production
- `npm run build` will output to `./dist`
