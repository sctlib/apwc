import AtpAuth from "./web-components/atp-auth.js";

/* all the exported components */
const componentDefinitions = {
	"atp-auth": AtpAuth,
};

/* auto define all components, if in browser */
const isBrowser = typeof window !== "undefined";

export function defineComponents(components) {
	if (!isBrowser) return;
	Object.entries(components).map(([cTag, cDef]) => {
		if (!customElements.get(cTag)) {
			customElements.define(cTag, cDef);
		}
	});
}

defineComponents(componentDefinitions);
