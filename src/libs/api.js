import { BskyAgent } from "@atproto/api";

class Api extends EventTarget {
	get user() {
		return this._user;
	}
	set user(user) {
		this._setUser(user);
	}
	constructor() {
		super();
		this._user = null;
		this.init();
	}
	async init() {
		try {
			this.db = await openDatabase();
			await this._getUser();
		} catch (e) {
			console.error(e);
		}
	}
	async _setUser(user) {
		if (!user) {
			await setData(this.db, "account", "user", null);
		} else {
			await setData(this.db, "account", "user", JSON.stringify(user));
		}
		this._user = user;

		this.dispatchEvent(
			new CustomEvent("auth", {
				bubbles: true,
				detail: user,
			}),
		);
	}
	async _getUser() {
		try {
			const user = await getData(this.db, "account", "user");
			if (user) {
				this._user = JSON.parse(user);
			}
		} catch (e) {
			console.error(e);
		}
	}

	// creat atproto agent from service URL
	createAgent(service) {
		this.agent = new BskyAgent({ service });
	}

	async login({ email, password }) {
		const loginRes = await this.agent.login({
			identifier: email,
			password: password,
		});
		const { data, success } = loginRes;
		if (success) {
			this.user = data;
		} else {
			this.user = null;
		}
		return success;
	}
	logout() {
		this.user = null;
	}
}

function openDatabase({
	version = 1,
	name = "@sctlib/atpwc",
	stores = ["account"],
} = {}) {
	return new Promise((resolve, reject) => {
		const request = indexedDB.open(name, version);
		request.onupgradeneeded = (event) => {
			const db = event.target.result;
			stores.forEach((storeName) => {
				if (!db.objectStoreNames.contains(storeName)) {
					db.createObjectStore(storeName);
				}
			});
		};
		request.onsuccess = (event) => {
			resolve(event.target.result);
		};
		request.onerror = (event) => {
			reject(event.error);
		};
	});
}
async function getData(db, storeName, key) {
	return new Promise((resolve, reject) => {
		const transaction = db.transaction(storeName, "readonly");
		const store = transaction.objectStore(storeName);
		const request = store.get(key);
		request.onsuccess = (event) => resolve(event.target.result);
		request.onerror = (event) => reject(event.error);
	});
}

async function setData(db, storeName, key, value) {
	return new Promise((resolve, reject) => {
		const transaction = db.transaction(storeName, "readwrite");
		const store = transaction.objectStore(storeName);
		const request = store.put(value, key);
		request.onsuccess = (event) => resolve();
		request.onerror = (event) => reject(event.error);
	});
}

const api = new Api();
export default api;
export { Api };
