import api from "../libs/api.js";

export default class AtpAuth extends HTMLElement {
	async connectedCallback() {
		api.addEventListener("auth", () => {
			this.render();
		});
		await api.init();
		this.render();
	}

	async login({ service, email, password }) {
		api.createAgent(service);
		return await api.login({
			email,
			password,
		});
	}

	async onLogin(event) {
		event.preventDefault();
		event.stopPropagation();
		const formData = new FormData(event.target);
		const email = formData.get("email");
		const password = formData.get("password");
		const service = formData.get("service");
		if (email && password && service) {
			const login = await this.login({ email, password, service });
		}
	}

	async onLogout(event) {
		event.preventDefault();
		event.stopPropagation();
		await api.logout();
	}

	render() {
		this.innerHTML = "";
		if (!api.user) {
			this.renderLogin();
		} else {
			this.renderLogout();
		}
	}
	renderLogin() {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onLogin.bind(this));

		const $fieldsetService = document.createElement("fieldset");
		const $legendService = document.createElement("legend");
		const $inputService = document.createElement("input");
		$legendService.innerText = "Service";
		$inputService.placeholder = "https://bsky.social";
		$inputService.value = "https://bsky.social";
		$inputService.name = "service";
		$fieldsetService.append($legendService, $inputService);

		const $fieldsetEmail = document.createElement("fieldset");
		const $legendEmail = document.createElement("legend");
		const $inputEmail = document.createElement("input");
		$legendEmail.innerText = "Email";
		$inputEmail.placeholder = "username@domain.tld";
		$inputEmail.type = "email";
		$inputEmail.name = "email";
		$fieldsetEmail.append($legendEmail, $inputEmail);

		const $fieldsetPw = document.createElement("fieldset");
		const $legendPw = document.createElement("legend");
		const $inputPw = document.createElement("input");
		$legendPw.innerText = "Password";
		$inputPw.placeholder = "***";
		$inputPw.type = "password";
		$inputPw.name = "password";
		$fieldsetPw.append($legendPw, $inputPw);

		const $fieldsetSubmit = document.createElement("fieldset");
		const $legendSubmit = document.createElement("legend");
		const $inputSubmit = document.createElement("button");
		$legendSubmit.innerText = "Login";
		$inputSubmit.type = "submit";
		$inputSubmit.innerText = "Login";
		$fieldsetSubmit.append($legendSubmit, $inputSubmit);

		$form.append(
			$fieldsetService,
			$fieldsetEmail,
			$fieldsetPw,
			$fieldsetSubmit,
		);
		this.append($form);
	}
	renderLogout() {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onLogout.bind(this));

		const $fieldsetHandle = document.createElement("fieldset");
		const $legendHandle = document.createElement("legend");
		const $inputHandle = document.createElement("input");
		$legendHandle.innerText = "Handle";
		$inputHandle.placeholder = api.user.handle;
		$inputHandle.readOnly = true;
		$inputHandle.type = "user";
		$inputHandle.name = "handle";
		$fieldsetHandle.append($legendHandle, $inputHandle);

		const $fieldsetSubmit = document.createElement("fieldset");
		const $legendSubmit = document.createElement("legend");
		const $inputSubmit = document.createElement("button");
		$legendSubmit.innerText = "Logout";
		$inputSubmit.type = "submit";
		$inputSubmit.innerText = "Logout";
		$fieldsetSubmit.append($legendSubmit, $inputSubmit);

		$form.append($fieldsetHandle, $fieldsetSubmit);
		this.append($form);
	}
}
